;;; early-init.el --- primeiras opções de inicialização

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Este é o arquivo de inicialização do Unimacs. Aqui são definidas
;; opções de inicialização rápida e inicialização do resto da
;; distribuição.

;;; Inicialização rápida
(setq gc-cons-threshold most-positive-fixnum
      gc-cons-percentage 0.6
	  startup-file-name-handler-alist file-name-handler-alist
      file-name-handler-alist nil)

(defun startup-reset-defaults ()
  (setq gc-cons-threshold 16777216
        gc-cons-percentage 0.1
        file-name-handler-alist startup-file-name-handler-alist))

(add-hook 'emacs-startup-hook #'startup-reset-defaults)

;;; Desabilitar `package.el'
(setq package-enable-at-startup nil
      package--init-file-ensured t)

