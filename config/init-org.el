;;; init-org.el --- configurações para o org-mode

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Este arquivo contém todas as extensões diretamente relacionadas ao
;; uso do `org-mode', incluindo o `org-roam', `deft', entre outros.

(use-package org
  :ensure t
  :init
  (setq org-export-backends '(beamer html latex md))
  :custom
  (org-directory (expand-file-name unimacs-org-directory))
  (org-ellipsis "⬎")
  (org-hide-leading-stars t)
  (org-html-postamble nil)
  (org-agenda-files (expand-file-name unimacs-org-agenda-file org-directory))
  (org-archive-location (expand-file-name "archive.org" org-directory))
  (org-enforce-todo-dependencies t)
  (org-enforce-todo-checkbox-dependencies t)
  (org-log-done 'time)
  (org-agenda-window-setup 'only-window)
  (org-link-frame-setup '((vm . vm-visit-folder-other-frame)
                          (vm-imap . vm-visit-imap-folder-other-frame)
                          (gnus . org-gnus-no-new-news)
                          (file . find-file)
                          (wl . wl-other-frame)))
  (org-src-tab-acts-natively t)
  (org-src-fontify-natively t)
  (org-src-window-setup 'current-window)
  (org-structure-template-alist
   '(("c" . "center\n")
     ("e" . "src emacs-lisp :tangle init.el\n")
     ("h" . "export html\n")
     ("l" . "export latex\n")
     ("q" . "quote\n")
     ("r" . "src R :results output :export results\n")
     ("p" . "src python\n")
     ("s" . "src")
     ("v" . "verse\n")))
  :bind
  (:map org-mode-map
		(("M-n" . org-forward-element)
		 ("M-p" . org-backward-element)
		 ("C-M-n" . org-metadown)
		 ("C-M-p" . org-metaup)
		 ("C-M-f" . org-metaright)
		 ("C-M-b" . org-metaleft)
		 ("<mouse-3>" . org-cycle))))

(use-package org-superstar
  :ensure t
  :hook
  (org-mode-hook . org-superstar-mode))

(use-package org-roam
  :ensure t
  :config
  (require 'org-protocol)
  (require 'org-roam-protocol)
  (org-roam-mode)
  :diminish org-roam-mode
  :custom
  (org-roam-directory (expand-file-name unimacs-org-roam-directory))
  (org-roam-encrypt-files nil)
  (org-roam-completion-system 'ivy)
  (org-roam-capture-templates
   '(("t" "tagged" plain (function org-roam--capture-get-point)
      "#+date:%T\n#+startup: overview\n"
      :file-name "%<%Y%m%d%H%M%S>-${slug}"
      :head "#+title: ${title}\n"
      :unnarrowed t)))
  :bind
  (:map notes
        (("f" . org-roam-find-file)
         ("l" . org-roam-insert)
         ("c" . org-roam-random-note)
         ("d" . org-roam-dailies-date))))

(use-package org-roam-server
  :ensure t
  :custom
  (org-roam-server-host "127.0.0.1")
  (org-roam-server-port 8080)
  (org-roam-server-authenticate nil)
  (org-roam-server-export-inline-images t)
  (org-roam-server-serve-files nil)
  (org-roam-server-served-file-extensions '("pdf" "mp4" "ogv"))
  (org-roam-server-network-poll t)
  (org-roam-server-network-arrows nil)
  (org-roam-server-network-label-truncate t)
  (org-roam-server-network-label-truncate-length 60)
  (org-roam-server-network-label-wrap-length 20)
  :bind
  (:map notes
        ("g" . org-roam-server-mode)))

(use-package deft
  :ensure t
  :custom
  (deft-recursive t)
  (deft-use-filter-string-for-filename t)
  (deft-default-extension "org")
  (deft-directory org-roam-directory)
  :bind
  (:map notes
		("s" . deft)))

(define-prefix-command 'notes)
(global-set-key (kbd "C-c n") 'notes)

(setq org-confirm-babel-evaluate nil)

(setq org-babel-load-languages
      '((R . t)
        (python . t)
        (emacs-lisp . t)
        (shell . t)
        (org . t)
        (latex . t)))

(provide 'init-org)
