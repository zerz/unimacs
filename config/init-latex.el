;;; init-latex.el --- editoração em LaTeX

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Neste arquivo configuramos o `dired', o gerenciador de arquivos do
;; Emacs.

(use-package auctex
  :ensure t
  :custom
  (TeX-PDF-mode t)
  (TeX-view-program-selection '((output-pdf "PDF Tools")))
  (TeX-source-correlate-mode t)
  (TeX-source-correlate-start-server t)
  :config
  (cond
   ((not (string-equal system-type "windows-nt"))
	(progn
	  (setq TeX-view-program-list
			'(("Sumatra PDF" ("\"C:/Program Files/SumatraPDF/SumatraPDF.exe\" -reuse-instance"
							  (mode-io-correlate " -forward-search %b %n ") " %o"))))
	  (eval-after-load 'tex
		'(progn
		   (assq-delete-all 'output-pdf TeX-view-program-selection)
		   (add-to-list 'TeX-view-program-selection '(output-pdf "Sumatra PDF")))))))

  :hook
  (TeX-after-compilation-finished-functions . TeX-revert-document-buffer))

(provide 'init-latex)
