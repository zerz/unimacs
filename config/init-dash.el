;;; init-dash.el --- tela de início

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Este arquivo configura a tela de início do Unimacs.

(require 'init-package)

;;; Desabilitar tela e mensagem de início
(setq inhibit-startup-message t)
(setq inhibit-startup-echo-area-message t)

(use-package dashboard
  :ensure t
  :defer nil
  :config
  (dashboard-setup-startup-hook)

  (defun unimacs/atualizar-configuração ()
	"Atualiza a configuração"
	(interactive)
	(let ((dir (expand-file-name user-emacs-directory)))
	  (if (file-exists-p dir)
		  (progn
			(message "Atualizando o Unimacs!")
			(cd dir)
			(shell-command "git pull")
			(message "Atualização finalizada com sucesso. Reinicie o Emacs."))
		(message "\"%s\" não existe." dir))))
  
  :custom
  (dashboard-set-heading-icons t)
  (dashboard-set-file-icons t)
  (dashboard-items
   '((bookmarks . 5)
	 (recents . 5)
	 (agenda . 10)))
  (dashboard-banner-logo-title "Bem-vindo ao Emacs!")
  (dashboard-startup-banner 'official)
  (dashboard-center-content t)
  (dashboard-show-shortcuts t)
  (dashboard-set-init-info t)
  (dashboard-set-footer t)
  (dashboard-footer-messages nil)
  (dashboard-set-navigator t)
  (dashboard-navigator-buttons
   `(;; line1
	 ((,nil
	   "Configuração"
	   "Abrir o arquivo de configuração para fácil modificação"
	   (lambda (&rest _) (find-file unimacs-user-config))
	   'default)
	  (nil
	   "Gitlab"
	   "Abrir a página do Unimacs no seu navegador de internet"
	   (lambda (&rest _) (browse-url "https://gitlab.com/aabm/unimacs"))
	   'default)
	  )
	 ;; line 2
	 ((,nil
	   "Tutorial"
	   "Abrir o tutorial do GNU Emacs"
	   (lambda (&rest _) (help-with-tutorial))
	   'default)
	 (nil
	   "Eshell"
	   "Abrir a Eshell"
	   (lambda (&rest _) (eshell))
	   'default)))))

  ;; Abrir o dashboard apenas se o Emacs não receber nenhum argumento na inicialização
  (if (< (length command-line-args) 2)
	  (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*"))))

(provide 'init-dash)
