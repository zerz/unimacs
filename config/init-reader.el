;;; init-reader.el --- leitores de pdf e epub

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Aqui configuramos leitores para documentos tipo PDF e EPUB, usando
;; os leitores `pdf-tools' e `nov.el'.

(use-package pdf-tools
  :ensure t
  :defer nil
  :config
  (pdf-loader-install)

  (defun unimacs/pdf-view-continuous-toggle ()
    (interactive)
    (cond ((not pdf-view-continuous)
           (setq pdf-view-continuous t)
           (message "Page scrolling: Continous"))
          (t
           (setq pdf-view-continuous nil)
           (message "Page scrolling: Constrained"))))

  :custom
  (pdf-view-resize-factor 1.1)
  (pdf-view-continuous nil)
  (pdf-view-display-size 'fit-page)
  :bind (:map pdf-view-mode-map
              ("C-s" . isearch-forward)
              ("C-r" . isearch-backward)
              ("C-c d" . pdf-view-midnight-minor-mode)
              ("C-c t" . unimacs/pdf-view-continuous-toggle )
              ("C-a" . image-scroll-right)
              ("C-e" . image-scroll-left)))
(use-package nov
  :ensure t
  :init
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
  (defun my-nov-setup ()
    "Setup `nov-mode' for better reading experience."
    (visual-line-mode 1)
    (unimacs/read-mode))
  :custom
  (nov-text-width 80)
  (nov-text-width t)
  (visual-fill-column-center-text t)  
  :hook
  (nov-mode-hook . my-nov-setup)
  :bind
  (:map nov-mode-map
        ((("M-n" . scroll-up-line)
          ("M-p" . scroll-down-line)))))


(define-minor-mode unimacs/read-mode
  "Minor Mode for better reading experience."
  :init-value nil
  :group unimacs
  (if unimacs/read-mode
      (progn
        (and (fboundp 'olivetti-mode) (olivetti-mode 1))
        (and (fboundp 'mixed-pitch-mode) (mixed-pitch-mode 1))
        (text-scale-set +1))
    (progn
      (and (fboundp 'olivetti-mode) (olivetti-mode -1))
      (and (fboundp 'mixed-pitch-mode) (mixed-pitch-mode -1))
      (text-scale-set 0))))

(provide 'init-reader)
