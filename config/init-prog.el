;;; init-prog.el --- opções básicas para programação

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Este arquivo configura quaisquer opções comuns para programação. Extensões específicas de linguagens estão em seus próprios arquivos.

(require 'init-package)
(require 'init-text)

(add-hook 'prog-mode-hook 'display-line-numbers-mode)

(global-set-key (kbd "C-c c") 'compile)

(use-package rainbow-delimiters
  :ensure t
  :hook
  ((prog-mode-hook . rainbow-delimiters-mode)))

(use-package company
  :ensure t
  :diminish company-mode
  :custom
  (company-idle-delay 0)
  (company-minimum-prefix-length 3)
  :bind
  ((:map company-active-map
         ("M-n" . nil)
         ("M-p" . nil)
         ("C-n" . company-select-next)
         ("C-p" . company-select-previous)
         ("SPC" . (lambda () (interactive) (progn (company-abort) (insert " "))))))

  :hook
  ((prog-mode-hook . company-mode)))


(provide 'init-programming)
