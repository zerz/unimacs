;;; init-misc.el --- opções miscelâneas, de qualidade de vida

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Este arquivo configura quaisquer opções que não se encaixam em
;; nenhum outro lugar. Aqui mudamos alguns padrões arcaicos do Emacs.

(require 'init-package)

;;; Re-habilita comandos desabilitados
;; Sabe-se lá porque esse comandos são desabilitados por padrão...
(put 'dired-find-alternate-file 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(setq disabled-command-function nil)

;;; Qualidade de vida
(setq scroll-conservatively 100)
(setq ring-bell-function 'ignore)

(setq select-enable-clipboard t)
(setq save-interprogram-paste-before-kill t)

(setq kill-buffer-query-functions nil)
(setq large-file-warning-threshold nil)
(global-auto-revert-mode t)

(setq-default custom-safe-themes t)
(defalias 'yes-or-no-p 'y-or-n-p)

(setq make-backup-files nil)
(setq auto-save-default nil)

(use-package time
  :commands world-clock display-time-world
  :custom
  (display-time-format "%a, %b %d %H:%M")
  (display-time-interval 60)
  (display-time-mail-directory nil)
  (display-time-default-load-average t)
  (zoneinfo-style-world-list
   '(("America/Los_Angeles" "-8 Seattle")
     ("America/New_York" "-5 New York")
     ("America/Sao_Paulo" "-3 Brasília")
     ("Europe/London" "+0 London")
     ("Europe/Brussels" "+1 Berlin")
     ("Europe/Moscow" "+3 Baghdad")
     ("Asia/Shanghai" "+8 Shanghai")
     ("Asia/Tokyo" "+9 Tokyo"))))

(use-package calendar
  :commands calendar
  :custom
  (calendar-week-start-day 0)
  (calendar-day-name-array ["Domingo" "Segunda" "Terça" "Quarta" 
                            "Quinta" "Sexta" "Sábado"])
  (calendar-month-name-array ["Janeiro" "Fevereiro" "Março" "Abril"
                              "Maio" "Junho" "Julho" "Agosto"
                              "Setembro" "Outubro" "Novembro" "Dezembro"]))

(provide 'init-misc)
