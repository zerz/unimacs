;;; init-minibuffer.el --- minibuffer e automcompletion

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Aqui configuramos tudo que se relaciona com o
;; minibuffer. Autocompletion com o Ivy, etc.

(require 'init-package)
(require 'init-base)

(use-package which-key
  :ensure t
  :diminish which-key-mode
  :init
  (which-key-mode)
  :custom
  (which-key-show-early-on-C-h t))

(use-package ivy
  :ensure t
  :diminish ivy-mode
  :init
  (ivy-mode)
  :custom
  (enable-recursive-minibuffers t))

(use-package counsel
  :ensure t
  :diminish counsel-mode
  :init
  (counsel-mode)
  :bind
  ((("C-s" . swiper)
    ("C-r" . swiper-backward)
	
    ("C-x C-f" . counsel-find-file)
    ("C-x b" . counsel-switch-buffer)
    ("C-x r b" . counsel-jump)
    ("M-x" . counsel-M-x)
	
    ("C-h f" . counsel-describe-function)
    ("C-h v" . counsel-describe-variable)
    ("C-h o" . counsel-describe-symbol))))

(use-package all-the-icons-ivy-rich
  :ensure t
  :diminish all-the-icons-ivy-rich-mode
  :init
  (all-the-icons-ivy-rich-mode))

(use-package ivy-rich
  :ensure t
  :diminish ivy-rich-mode
  :init
  (ivy-rich-mode))

(provide 'init-minibuffer)
