;;; init-gui.el --- opções da interface gráfica em geral

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Este arquivo configura as opções da interface gráfica, como barras
;; de ferramentas, menu e scroll. Quaisquer opções relacionadas ao uso
;; do mouse também se encontram aqui.

;;; Opções da GUI
(push '(menu-bar-lines . 0) default-frame-alist)
(set-window-scroll-bars (minibuffer-window) nil nil)
(setq frame-inhibit-implied-resize t)

;;; Mouse e janelas
(setq focus-follows-mouse t
      mouse-autoselect-window t)

(provide 'init-gui)
