;;; init-text.el --- opções básicas de edição de texto

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE. If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Este arquivo controla as opções mais simples que envolvem a
;; manipulação de texto, como encoding UTF-8 e indentação.

(require 'init-package)
(require 'init-base)

;;; Sempre utilizar UTF-8
(prefer-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(set-language-environment 'utf-8)
(set-default-coding-systems 'utf-8)
(setq locale-coding-system 'utf-8
	  org-export-coding-system 'utf-8)

;;; Indentação genérica
(setq-default tab-width 4)

;;; Números de linha em modos de programação
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;;; Continuidade de linhas longas
;; Estas opções fazem com que linhas longas sejam exibidas
;; completamente, ao invés de continuarem para além da janela.
(setq truncate-lines nil)
(setq org-startup-truncated nil)

;;; Opções de exibição de texto
(show-paren-mode 1)

(global-visual-line-mode t)

(global-prettify-symbols-mode t)

;;; Extensões de edição de texto
(setq electric-pair-pairs '((?\{ . ?\}) (?\( . ?\))
							(?\[ . ?\]) (?\" . ?\")))

(electric-pair-mode t)

(use-package undo-tree
  :ensure t
  :diminish undo-tree-mode
  :init
  (global-undo-tree-mode)
  :custom
  (undo-tree-visualizer-timestamps t)
  (undo-tree-enable-undo-in-region nil)
  (undo-tree-auto-save-history nil))

(use-package expand-region
  :ensure t
  :bind
  (("C-=" . er/expand-region)))

(use-package olivetti
  :ensure t
  :custom
  (olivetti-body-width 100)
  :bind
  (("C-c o" . olivetti-mode)))

(use-package writeroom-mode
  :ensure t
  :bind
  (("C-c w" . writeroom-mode)))

(use-package delsel
  :init
  (delete-selection-mode t))

(use-package subword
  :diminish subword-mode
  :hook ((prog-mode-hook . subword-mode)
         (minibuffer-setup-hook . subword-mode)))

(use-package auto-sudoedit
  :ensure t
  :diminish
  :init
  (auto-sudoedit-mode))

(use-package markdown-mode
  :ensure t)

(provide 'init-text)
