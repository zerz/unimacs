;;; init-dired.el --- dired, editor de diretório

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Neste arquivo configuramos o `dired', o gerenciador de arquivos do
;; Emacs.

(require 'init-package)
(require 'init-theming)

(use-package dired-subtree :ensure t)
(use-package dired-hide-dotfiles :ensure t)

(use-package dired
  :commands dired
  :custom
  (dired-dwim-target t)
  (dired-recursive-copies 'always)
  (dired-recursive-deletes 'always)  
  (dired-listing-switches "-alhvNF --group-directories-first")
  (wdired-allow-to-change-permissions t)
  
  :config
  (defun unimacs/dired-xdg-open ()
    "Open the marked files using xdg-open."
    (interactive)
    (let ((file-list (dired-get-marked-files)))
      (mapc
       (lambda (file-path)
         (let ((process-connection-type nil))
           (start-process "" nil "xdg-open" file-path)))
       file-list)))

  (defun unimacs/dired-up-alternate-directory ()
    "Move up a directory, reusing the current buffer, instead of creating a new one."
    (interactive)
    (find-alternate-file ".."))

  :hook
  (((dired-mode-hook . dired-hide-dotfiles-mode)
    (dired-mode-hook . hl-line-mode)))

  :diminish dired-hide-dotfiles-mode

  :bind
  (:map dired-mode-map
        ((("RET" . dired-find-alternate-file)
          ("M-RET" . dired-find-file)
          ("DEL" . unimacs/dired-up-alternate-directory)
          ("l" . unimacs/dired-up-alternate-directory)
          ("TAB" . dired-subtree-cycle)
          ("M-n" . dired-subtree-down)
          ("M-p" . dired-subtree-up)
          ("C-c d m" . mkdir)
          ("C-c d c" . chmod)
          ("h" . dired-hide-dotfiles-mode)
          ("H" . dired-hide-details-mode)
          ("I" . image-dired)
          ("v" . unimacs/dired-xdg-open)
          ("q" . kill-this-buffer)))))

(use-package diredfl
  :ensure t
  :init
  (diredfl-global-mode t))

(use-package all-the-icons-dired
  :ensure t)

(use-package dired-sidebar
  :ensure t
  :commands
  (dired-sidebar-toggle-sidebar)
  :config
  (push 'toggle-window-split dired-sidebar-toggle-hidden-commands)
  (push 'rotate-windows dired-sidebar-toggle-hidden-commands)
  :custom
  (dired-sidebar-subtree-line-prefix "__")
  (dired-sidebar-theme 'icons)
  (dired-sidebar-use-term-integration t)
  (dired-sidebar-use-custom-font t)
  :bind
  (("C-x C-n" . dired-sidebar-toggle-sidebar)))

(provide 'init-dired)
