;;; init-buffer.el --- manipulação de buffers

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Este arquivo define funções e configura pacotes para manipulação e
;; gerenciamento de buffers.

(require 'init-package)
(require 'init-base)
(require 'init-theming)

(use-package all-the-icons-ibuffer
  :ensure t)

(use-package ibuffer
  :ensure t
  :defer nil
  :init
  (setq ibuffer-filter-group-name-face
		'(:inherit (font-lock-string-face bold)))
  :hook
  ((ibuffer-mode-hook . all-the-icons-ibuffer-mode))
  :bind
  (("C-x C-b" . ibuffer)))

(use-package ibuffer-projectile
  :ensure t
  :hook
  ((ibuffer-hook . (lambda ()
                     (ibuffer-projectile-set-filter-groups)
                     (unless (eq ibuffer-sorting-mode 'alphabetic)
                       (ibuffer-do-sort-by-alphabetic)))))
  :config
  (setq ibuffer-projectile-prefix
        (if (icons-displayable-p)
            (concat
             (all-the-icons-octicon "file-directory"
                                    :face ibuffer-filter-group-name-face
                                    :v-adjust 0.0
                                    :height 1.0)
             " "))))

(defun genbuffer-org ()
  "Create and switch to a temporary org mode buffer with a random name."
  (interactive)
  (switch-to-buffer (make-temp-name "org-"))
  (org-mode))

(defun genbuffer-scratch ()
  "Create and switch to a temporary scratch buffer with a random name."
  (interactive)
  (switch-to-buffer (make-temp-name "scratch-"))
  (lisp-interaction-mode))

(defun genbuffer-text ()
  "Create and switch to a temporary text buffer with a random name."
  (interactive)
  (switch-to-buffer (make-temp-name "text-"))
  (fundamental-mode))

(defun kill-this-buffer-and-window ()
  "Kill the current buffer and its corresponding window."
  (interactive)
  (progn
    (kill-buffer)
    (delete-window)))

(global-set-key (kbd "C-x k") 'kill-this-buffer)
(global-set-key (kbd "C-x C-k") 'kill-this-buffer-and-window)

(define-prefix-command 'buffers)
(global-set-key (kbd "C-c b") 'buffers)

(define-key buffers (kbd "o") 'genbuffer-org)
(define-key buffers (kbd "t") 'genbuffer-text)
(define-key buffers (kbd "s") 'genbuffer-scratch)

(provide 'init-buffer)
