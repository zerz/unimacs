;;; init-base.el --- instalação de pacotes base

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Este arquivo configura os pacotes que servem de base para a
;; distribuição. Todo o resto da configuração depende destes pacotes.

(require 'init-package)

(use-package diminish
  :ensure t
  :defer nil
  :diminish visual-line-mode eldoc-mode org-indent-mode)

(use-package async
  :ensure t
  :init
  (dired-async-mode t)
  (async-bytecomp-package-mode t)
  :diminish dired-async-mode)

(use-package gcmh
  :ensure t
  :init
  (gcmh-mode)
  :diminish gcmh-mode
  :custom
  (gcmh-verbose t))

(use-package which-key
  :ensure t
  :init
  (which-key-mode)
  :diminish which-key-mode
  :custom
  (which-key-show-early-on-C-h t))

(use-package projectile
  :ensure t
  :init
  (projectile-mode)
  :bind
  (("C-c p" . projectile-command-map)))

(provide 'init-base)
