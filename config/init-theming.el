;;; init-theming.el --- tema e customização

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Este arquivo configura e customiza toda a aparência do Unimacs,
;; exceto pela tela de início.

(require 'init-package)
(require 'init-base)

(use-package beacon
  :ensure t
  :diminish beacon-mode
  :init
  (beacon-mode)
  :config
  (blink-cursor-mode -1))

(use-package modus-operandi-theme
  :ensure t
  :custom
  (modus-operandi-theme-no-mixed-fonts nil)
  (modus-operandi-theme-proportional-fonts nil)
  (modus-operandi-theme-org-blocks 'greyscale))

(use-package modus-vivendi-theme
  :ensure t
  :custom
  (modus-vivendi-theme-no-mixed-fonts nil)
  (modus-vivendi-theme-proportional-fonts nil)
  (modus-vivendi-theme-org-blocks 'greyscale))

(use-package all-the-icons
  :ensure t
  :defer nil)

(defun icons-displayable-p ()
  "Return non-nil if `all-the-icons' is displayable."
  (if (display-graphic-p)
      (require 'all-the-icons nil t)))

(load-theme 'modus-operandi t)

(provide 'init-theming)
