;;; unimacs.el --- configuração do usuário

;;; Credenciais do usuário
(setq unimacs-user-full-name "Seu Nome Aqui"
	  unimacs-mail-address "nome@email.net"
	  unimacs-calendar-latitude 00.000
	  unimacs-calendar-longitude 00.000
	  unimacs-location-name "Cidade, UF")

;;; Diretórios do usuário
(setq unimacs-org-directory "~/Documentos/Org"
	  unimacs-org-agenda-file "agenda.org"
	  unimacs-org-roam-directory "~/Documentos/Roam")

(provide 'unimacs-user)
