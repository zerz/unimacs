;;; init.el --- principais opções de inicialização

;; Copyright © 2019-2021 Aabm <aabm@disroot.org>

;; Author: Aabm <aabm@disroot.org>
;; URL: https://gitlab.com/aabm/unimacs
;; Keywords: emacs academic writing

;; ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣞⠟⠉⠉⠛⠳⢶⣄⡀
;; ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠙⠻⠶⢦⣤⡀⠀⠈⠛⢶⣶⣶⣶⣶⣶⣦⣤⣄⡀
;; ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣠⣔⣶⣿⣿⣿⣿⣷⣢⣀⢈⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣤⣄⡀
;; ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⣠⣴⣿⣿⣿⣿⣿⠿⢟⣿⣿⣿⣷⣿⣿⣿⣿⣿⣿⣿⡿⠿⠟⠉⠀⢀⣿⣿⣢⣀
;; ⠀⠀⠀⠀⠀⠀⠀⠀⣠⣾⣾⣿⣿⣿⣿⠏⠀⠀⠀⠈⢻⣿⣿⣿⣿⣿⣟⡅⠀⠀⠀⠀⢀⣤⣶⣿⣿⣿⣿⣿⣷⣄
;; ⠀⠀⠀⠀⠀⠀⢀⣼⣿⣿⣿⣿⣿⣿⣿⠀⠀⠀⠀⠀⠈⣾⣿⣿⣿⣿⣿⣿⣿⣿⡻⠿⠷⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀
;; ⠀⠀⠀⠀⠀⢠⣻⣿⣿⣿⣿⣿⣿⣿⣿⡀⠀⠀⠀⠀⢠⣿⣿⣿⣿⣿⣿⣿⣯⠋⠀⠀⠀⠈⢟⣿⣿⣿⣿⣿⣿⣿⣿⣷⡄
;; ⠀⠀⠀⠀⢠⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡦⣀⣀⣴⣿⣿⣿⣿⣿⣿⣿⣿⡎⠀⠀⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣾⡀
;; ⠀⠀⠀⠀⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⡀⠀⠀⠀⠀⣜⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷
;; ⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⣤⢄⣤⣺⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡇
;; ⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠟⠛⠛⠛⠻⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣾
;; ⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢻⠋⠁⢀⣀⣀⡀⠀⠀⠀⠀⠉⠻⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
;; ⠀⠀⢠⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣶⣿⣿⣿⣿⣿⣿⣶⣶⣤⣄⡀⠀⠻⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄
;; ⠀⠀⠈⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣧⣄⣸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠁
;; ⠀⠀⢀⡠⣤⡉⠺⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣟⠿⠿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿
;; ⠀⠀⢸⣿⣿⣿⣦⠘⣿⣿⣿⣿⣿⣿⣿⣿⣿⠿⠟⢋⣉⣤⣤⣤⣌⠙⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡻
;; ⠀⠀⠘⣽⣿⣿⣿⡇⢸⢿⣿⣿⢿⡿⠟⢉⡠⣴⣮⣿⣿⣿⣿⣿⣿⠇⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠇
;; ⠀⠀⠀⢸⣿⣿⣿⣟⡄⠛⠛⢉⣡⣔⣮⣿⣿⣿⣿⣿⣿⣿⡻⠟⠋⡠⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡝
;; ⠀⠀⠀⢨⣿⣿⣿⣿⣿⣷⣽⣿⣿⣿⣿⣿⣿⣿⠿⠟⠓⣉⣤⣲⣿⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟⠁
;; ⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣷⠀⢺⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡟
;; ⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⣾⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠋
;; ⠀⠀⠀⠘⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡆⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠟⠁
;; ⠀⠀⠀⠀⢟⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠯⠋
;; ⠀⠀⠀⠀⠈⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡿⠃⣸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢿⣿⠯⠛⠉
;; ⠀⠀⠀⠀⠀⠀⠙⠿⢿⣿⣿⢿⣿⠿⠯⠛⠋⠉⠀⠀⠉⠚⠛⠛⠛⠛⠛⠛⠓⠉⠉⠁
;; ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠈⠉⠁

;;; Licensa
;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file LICENSE.  If not, you can visit
;; https://www.gnu.org/licenses/gpl-3.0.html or write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Comentários
;; Este é o arquivo de inicialização do `Unimacs'. A partir daqui é
;; carregado todo o resto desta configuração.

;;; Versão do Emacs
;; Esta configuração suporta apenas a mais recente versão estável do
;; GNU Emacs.
(when (version< emacs-version "27.1")
  (error "Esta configuração requer o Emacs em versão 27.1 ou acima!"))

;;; Configurando `load-path'
(defun update-load-path (&rest _)
  "Update `load-path'."
  (dolist (dir '("config"))
    (push (expand-file-name dir user-emacs-directory) load-path)))

(defun add-subdirs-to-load-path (&rest _)
  "Add subdirectories to `load-path'."
  (let ((default-directory (expand-file-name "config" user-emacs-directory)))
    (normal-top-level-add-subdirs-to-load-path)))

(advice-add #'package-initialize :after #'update-load-path)
(advice-add #'package-initialize :after #'add-subdirs-to-load-path)

(update-load-path)

;;; Carrega pacotes de configuração própria
(load-file (expand-file-name "unimacs.el" user-emacs-directory))

(require 'init-package)
(require 'init-gui)

(require 'init-base)
(require 'init-text)
(require 'init-misc)
(require 'init-minibuffer)

(require 'init-dash)
(require 'init-modeline)
(require 'init-theming)

(require 'init-buffer)
(require 'init-window)

(require 'init-shell)
(require 'init-dired)
(require 'init-vcs)

(require 'init-org)
(require 'init-latex)
(require 'init-prog)
(require 'init-ess)
(require 'init-python)

(cond
 ((not (string-equal system-type "windows-nt"))
  (progn
    (require 'init-reader))))

(server-start)
